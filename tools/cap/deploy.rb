lock '3.5.0'

set :application, 'npc-website'
set :repo_url, 'git@bitbucket.org:codigo5/npc-website.git'

set :linked_files, fetch(:linked_files, []).push('.env')
set :linked_dirs, fetch(:linked_dirs, []).push('web/app/uploads')

after 'deploy:starting', 'composer:install_executable'
# after 'deploy:publishing', 'deploy:update_option_paths'
after 'deploy:symlink:release', 'symlink:public'


namespace :cod5 do
    desc 'cod5 config'
    task :clean do
    on roles(:web) do
        execute 'echo ------------******* CODIGO5 *******------------'
        execute 'echo ------------******* APAGAR ARQUIVOS DO SERVIDOR *******------------'
        
        execute "rm -Rf #{fetch :deploy_to}/public_html"
        execute "rm -Rf #{fetch :deploy_to}/html"
        execute "rm -Rf #{fetch :deploy_to}/iworx-backup"
        execute "rm -Rf #{fetch :deploy_to}/repo"
        execute "rm -Rf #{fetch :deploy_to}/current"
        execute "rm -Rf #{fetch :deploy_to}/releases"
        execute "rm -Rf #{fetch :deploy_to}/revisions.log"
        
        # rm -Rf html iworx-backup repo current releases revisions.log 
    end
     end
end

before "deploy", "cod5:clean"

SSHKit.config.command_map[:composer] = -> { "php #{shared_path.join("composer.phar")}" }
